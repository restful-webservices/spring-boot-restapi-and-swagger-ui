package com.codingraja.repository;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.codingraja.domain.Customer;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
	
	@Autowired
	private final SessionFactory sessionFactory;

	public CustomerRepositoryImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	@Override
	public Customer save(Customer customer) {
		Long id = null;
		if(customer.getId() == null)
			id = (Long)sessionFactory.getCurrentSession().save(customer);
		else{
			sessionFactory.getCurrentSession().update(customer);
			id = customer.getId();
		}
		return sessionFactory.getCurrentSession().get(Customer.class, id);
	}

	@Override
	public Customer findOne(Long id) {
		return sessionFactory.getCurrentSession().get(Customer.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> findAll() {
		return sessionFactory.getCurrentSession().createCriteria(Customer.class).list();
	}

	@Override
	public void delete(Long id) {
		Customer customer = sessionFactory.getCurrentSession().get(Customer.class, id);
		sessionFactory.getCurrentSession().delete(customer);
	}

}
